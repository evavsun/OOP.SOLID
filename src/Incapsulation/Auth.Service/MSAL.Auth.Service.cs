﻿using System;

namespace Auth.Service
{
	public class MSALService
	{
        public void GetLogger() { }
        public bool IsCallback { get; set; }
        public string GetCachedTokenInternal() { return "Bearer token"; }
        // some functionality ...
        public void Login() { }
        public void Logout() { }
    }
}
