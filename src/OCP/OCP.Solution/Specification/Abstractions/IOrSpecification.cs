﻿namespace OCP.Solution.Specification
{
    public interface IOrSpecification<T, in U> : ISpecification<T>
    {
        bool OrSatisfyedBy(U item);
    }
}