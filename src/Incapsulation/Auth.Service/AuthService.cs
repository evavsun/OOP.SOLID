﻿using System;
namespace Auth.Service
{
    public class AuthService
    {
        private readonly MSALService mSALService;

        public AuthService()
        {
            this.mSALService = new MSALService();
        }

        public void Login()
        {
            this.mSALService.Login();
        }

        public void Logout()
        {
            this.mSALService.Logout();
        }
    }
}
