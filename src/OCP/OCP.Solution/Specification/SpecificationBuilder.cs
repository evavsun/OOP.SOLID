﻿using System;

namespace OCP.Solution.Specification
{
    public class SpecificationBuilder<T>
    {
        private Predicate<T> filter;

        public SpecificationBuilder<T> FilterBy(Predicate<T> filter)
        {
            this.filter = filter;
            return this;
        }

        public ISpecification<T> Build()
        {
            return new SearchPatternSpec<T>(filter);
        }
    }

    public class SearchPatternSpec<T> : ISpecification<T>
    {
        private readonly Predicate<T> predicate;

        public SearchPatternSpec(Predicate<T> predicate)
        {
            this.predicate = predicate;
        }

        public bool IsSatisfiedBy(T entity)
        {
            return this.predicate(entity);
        }
    }
}