﻿using System;

namespace OCP.Solution.Specification
{
    public static class SpecificationExtensions
    {
        public static ISpecification<T> Or<T>(this ISpecification<T> spec, ISpecification<T> another)
        {
            ISpecification<T> r = new OrSpecification<T>(spec, another);
            return r;
        }
    }

    internal class OrSpecification<T> : ISpecification<T>
    {
        private readonly ISpecification<T> spec1;
        private readonly ISpecification<T> spec2;

        public OrSpecification(ISpecification<T> spec1, ISpecification<T> spec2)
        {
            this.spec1 = spec1;
            this.spec2 = spec2;
        }

        public bool IsSatisfiedBy(T entity)
        {
            return spec1.IsSatisfiedBy(entity) || spec2.IsSatisfiedBy(entity);
        }
    }
}