﻿using System;

namespace OCP.Solution.Specification
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T entity);
    }
}