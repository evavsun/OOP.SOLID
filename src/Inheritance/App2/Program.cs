﻿using App.Abstractions;
using App.Data;
using App.EventHandlers;
using App.Events;

namespace App
{
    internal class Program
    {
        private static void Main()
        {
            var eventBus = new EventBus(); // LoggingEventBus

            // LoggingEventBus logginEventBus = new LoggingEventBus();

            var productPurchased = new ProductPurchasedEvent(productId: "KK-4311", quantity: 10);

            // event hadler 
            // AbstractEventHandler<ProductPurchasedEvent> eventHadler = new PurchasedProductEventHandler();
            // eventHadler.Handle(productPurchased);

            eventBus.Publish(productPurchased);

            System.Console.WriteLine($"Loggin event bus {new string('-', 50)}");
            // logginEventBus.Publish(productPurchased);
        }
    }
}
