﻿using System;
using System.Linq;
using Sales;

namespace App
{
    internal class Program
    {
        private static void Main()
        {
            var customer = new Customer
            {
                FirstName = "John",
                LastName = "Smith"
            };

            var customer2 = new Customer
            {
                FirstName = "Jack",
                LastName = "Smiththesecond"
            };

            var manager = new CustomersManager();

            manager.AddCustomer(customer);
            manager.AddCustomer(customer2);

            var found = manager.FindCustomers("s").ToArray();

            Array.ForEach(found, Console.WriteLine);
        }
    }
}
