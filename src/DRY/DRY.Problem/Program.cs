﻿using System;
using System.Globalization;

namespace DRY.Problem
{
    public enum DateTimeFormat 
    {
        Local,
        Utc
    }

    internal class Program
    {
        private static void Main()
        {
            // var format = "Local";

             // var now = format == "Local"
                //? DateTime.Now.ToString(CultureInfo.InvariantCulture)
                //: format == "Utc"
                    //? DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)
                    //: throw new NotSupportedException($"Format {format} is not supported");

            Console.WriteLine(GetDateTimeInFormat(DateTimeFormat.Local));
            Console.WriteLine(GetDateTimeInFormat(DateTimeFormat.Local));
            Console.WriteLine(GetDateTimeInFormat(DateTimeFormat.Utc));


            // do something...

            //format = "Local";

            //now = format == "Local"
            //    ? DateTime.Now.ToString(CultureInfo.InvariantCulture)
            //    : format == "Utc"
            //        ? DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)
            //        : throw new NotSupportedException($"Format {format} is not supported");

            //Console.WriteLine(now);

            //// do something

            //format = "Utc";

            //now = format == "Local"
            //    ? DateTime.Now.ToString(CultureInfo.InvariantCulture)
            //    : format == "Utc"
            //        ? DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)
            //        : throw new NotSupportedException($"Format {format} is not supported");

            //Console.WriteLine(now);
        }

        private static string GetDateTimeInFormat(DateTimeFormat format)
        {
            return format == DateTimeFormat.Local
                    ? DateTime.Now.ToString(CultureInfo.InvariantCulture)
                    : format == DateTimeFormat.Utc
                        ? DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)
                        : throw new NotSupportedException($"Format {format} is not supported");

        }
    }
}
