﻿using KISS.App.Problem.CommunicationFramework;

namespace KISS.App.Problem
{
    public class WeatherApiSettings
    {
        public ISerializer Serializer { get; set; }
    }
}