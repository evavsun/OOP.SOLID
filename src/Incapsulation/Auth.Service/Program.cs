﻿using System;

namespace Auth.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            AuthService authService = new AuthService();
            authService.Login();
            authService.Logout();
        }
    }
}
