﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace Application
{
    internal interface ICommand
    {
        void Execute();
        void Undo();
    }

    //Concrete command
    internal class PushCommand : ICommand
    {
        private readonly Receiver receiver;

        public PushCommand(Recaiver receiver)
        {
            this.receiver = receiver;
        }

        public void Execute()
        {
            // do domething 
            this.receiver.SomeAction();
        }

        public void Undo()
        {
            throw new NotImplementedException();
        }
    }

    // Command's receiver
    internal class Receiver
    {
        public void SomeAction()
        {
            // do something ...
        }
    }

    // Command's Invoker
    internal class CommandProcessor
    {
        private readonly ICommand command;

        public CommandProcessor(ICommand command)
        {
            this.command = command;
        }

        public void Process()
        {
            this.command.Execute();
        }
    }
}
